<!-- Modal -->
<div class="modal fade" id="cheetahStatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <p> <span class="badge badge-primary">廃番</span>
                <small> 下記の商品を、 廃番 にしても良い場合は、 確定 ボタンを押してください。</small> 
            </p>
            <p> <span class="badge badge-warning">閉じる</span>
                <small> 廃番にしない場合は、閉じる ボタンを押してください。。</small>
            </p>
            <input type="hidden" name="id-change" id="id-change" value="">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
          <button type="button" id="submit-action" class="btn btn-primary">廃番</button>
        </div>
      </div>
    </div>
</div>