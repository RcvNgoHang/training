<!-- Modal -->
<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="addProductForm" action="{{asset(route('admin.addProduct'))}}" method="POST">
            @csrf
            <div class="form-group row">
              <label for="staticEmail" class="col-sm-2 col-form-label">Product code</label>
              <div class="col-sm-10">
                <input type="text"  class="form-control" id="product-code" name="product-code" value="">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 col-form-label">Product name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="product-name" placeholder="product name">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 col-form-label">Product jan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="product-jan" placeholder="Product jan">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 col-form-label">Product jan</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="cheetah-status" placeholder="Cheetah status">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 col-form-label">Process status</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="process-status" placeholder="Cheetah status">
              </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword" class="col-sm-2 col-form-label">Maker full name</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="maker-full-nm" placeholder="Maker full name">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal">閉じる</button>
          <button type="button" id="btn-add-product" class="btn btn-primary">廃番</button>
        </div>
      </div>
    </div>
</div>