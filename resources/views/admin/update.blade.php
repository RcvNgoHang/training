@extends('adminlte::page')
@section('title', 'Dashboard')
@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" integrity="sha512-fRVSQp1g2M/EqDBL+UFSams+aw2qk12Pl/REApotuUVK1qEXERk3nrCFChouag/PdDsPk387HJuetJ1HBx8qAg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('css/style.css') }}">
@endsection
@section('content')
@if ($errors->any()) 
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        
        <strong>Warning!</strong> 
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="container">
<form action="{{route('admin.update')}}" method="post">
    @csrf
    <div class="form-group row">
      <label for="staticEmail" class="col-sm-2 col-form-label">Product code</label>
      <div class="col-sm-10">
        <input type="text" readonly class="form-control" id="product-code" name="product-code" value="@if($product->product_code){{$product->product_code }} @endif">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Product name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="product-name" placeholder="product name" value="@if($product->product_name){{$product->product_name }} @endif">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Product jan</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="product-jan" placeholder="Product jan" value="@if($product->product_jan){{$product->product_jan }} @endif">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Cheetah status</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="cheetah-status" placeholder="Cheetah status" value="@if($product->cheetah_status){{$product->cheetah_status }} @endif">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Process status</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="process-status" placeholder="Cheetah status" value="@if($product->process_status){{$product->process_status }} @endif">
      </div>
    </div>
    <div class="form-group row">
      <label for="inputPassword" class="col-sm-2 col-form-label">Maker full name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" name="maker-full-nm" placeholder="Maker full name" value="@if($product->maker_full_nm){{$product->maker_full_nm }} @endif">
      </div>
    </div>
    <div class="form-group row">
        <button type="submit" class="btn btn-primary">update</button>
      </div>
  </form>
</div>


@stop
@section('js')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js" integrity="sha512-iceXjjbmB2rwoX93Ka6HAHP+B76IY1z0o3h+N1PeDtRSsyeetU3/0QKJqGyPJcX63zysNehggFwMC/bi7dvMig==" crossorigin="anonymous"></script> --}}
{{-- <script type="text/javascript" src="{{asset('js/bootstrap.bundle.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> 

@endsection