@extends('adminlte::page')
@section('title', 'Dashboard')
@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" integrity="sha512-fRVSQp1g2M/EqDBL+UFSams+aw2qk12Pl/REApotuUVK1qEXERk3nrCFChouag/PdDsPk387HJuetJ1HBx8qAg==" crossorigin="anonymous" />
    <link rel="stylesheet" href="{{asset('css/style.css') }}">
@endsection
@section('content')
@if ($errors->any()) 
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        
        <strong>Warning!</strong> 
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (session('status'))
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        
        <strong>Warning!</strong> 
        {{ session('status') }}
    </div>
@endif
<section class="search-form">
    <form  method="get" id="search-form">
    @csrf
    <div class="row">
        <div class="col-2">
            <div class="form-check">
                <input type="checkbox" class="form-check-input cheetah-status"  id="cheetah-status" name="cheetah-status[]" value="2">
                <label class="form-check-label" for="exampleCheck1"> 廃番 </label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input cheetah-status" id="exampleCheck1" name="cheetah-status[]" value="1">
                <label class="form-check-label" for="exampleCheck1">欠品 </label>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input cheetah-status" id="exampleCheck1" name="cheetah-status[]" value="0">
                <label class="form-check-label" for="exampleCheck1">販売</label>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <label class="form-check-label">JANコード</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" class="form-control" id="product-jan" placeholder="JANコード" id="product-jan" name="product-jan">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <label class="form-check-label">メーカー名</label>
                </div>
                <div class="col-sm-12">
                    <input id="poduct-maker-full" name="poduct-maker-full" type="text" class="form-control" id="inputEmail3" placeholder="メーカー名">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <label class="form-check-label">品番</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" id="product-code" name="product-code" class="form-control" id="inputEmail3" placeholder="品番">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <div class="col-sm-12">
                <label class="form-check-label">商品名</label>
                </div>
                <div class="col-sm-12">
                    <input type="text" id="product-name" name="product-name" class="form-control" id="inputEmail3" placeholder="商品名">
                </div>
            </div>
        </div>
        <div class="col-2">
            
            <div class="form-group row ">
                
                <div class="col-sm-6">
                    <button type="submit" class="btn btn-primary btn-sm"> 検索する</button>
                </div>
                <div class="col-sm-6">
                    <button type="button" id="reset-form" class="btn btn-secondary btn-sm">元に戻す</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-end">
        <div class="col-6 justify-content-end">
            <div class="form-group row">
                <div class="col-sm-2">
                    <button type="button" id="change-status" class="btn btn-danger btn-sm" > 検索する</button>
                </div>
                <div class="col-sm-2">
                    <button type="button" id="status-two" class="btn btn-warning btn-sm">元に戻す</button>
                </div>
                <div class="col-sm-2">
                    <button type="button" id="status-three" class="btn btn-success btn-sm">元に戻す</button>
                </div>
                <div class="col-sm-2">
                    {{-- id="export-csv"  --}}
                    <a type="button" href="{{route('admin.export')}}"   class="btn btn-success btn-sm">exportCSV</a>
                </div>
                
                
            </div>
        </div>
    </div>
    </form>
    <form action="{{route('admin.import')}}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col d-flex">
                {{-- id="export-csv"  --}}
                <input id="user-file" type="file" name="user_file" class="hidden" accept=".xlsx, .xls, .csv, .ods">
                <button type="submit" href="#" class="btn btn-dark btn-sm">importCSV</a>
            </div>
            <div class="col ">
                {{-- id="export-csv"  --}}
                <button type="button" href="#" id="add-new-product" class="btn btn-info btn-sm">Add new</a>
            </div>
        </div>
    </form>
</hr>
</section>

{{-- <form action="{{route('admin.export')}}" method="get">

</form> --}}
<div class="row">
   
    
    <table class="table table-bordered">
    <thead class="thead-dark">
        <tr>
        <th>ステータス</th>
        <th>メーカー名</th>
        <th>品番</th>
        <th>JANコード</th>
        <th>商品名</th>
        <th>定価</th>
        <th>処理ステータス</th>
        <th> 出荷指示
            +全て選択
            -選択解除</th>
        <th>Status</th>
       
        </tr>
    </thead>
    <tbody>
        <tr>
        @if( count($quertResult) > 0 )
        @foreach ($quertResult as $key => $value) 
        <td>
            <?php $status  = $value->cheetah_status ?? 0?>
            @if($status == 0)
            <span class="badge badge-success">販売</span>
            @elseif($status == 1)
            <span class="badge badge-warning">欠品</span>
            @else
            <span class="badge badge-danger">廃盤</span>
            @endif
        </td>
        <td>{{  $value->maker_full_nm}}</td>
        <td>{{  $value->product_code}}</td>
        <td>{{  $value->product_jan}}</td>
        <td>{{  $value->product_name}}</td>
        <td>{{  $value->list_price??0}}</td>
        <td>
            <?php $process_status  = $value->process_status ?? 0?>
            @if($process_status == 0)
            <span class="badge badge-success">Default</span>
            @elseif($status == 1)
            <span class="badge badge-warning">Waitting</span>
            @else
            <span class="badge badge-danger">Approved</span>
            @endif
        </td>
        <td>
            <div class="form-check mb-2 text-center">
                <input type="checkbox" class="form-check-input filled-in" name="status-change" id="status-change" value="{{  $value->product_code}}">
            </div>
        </td>
        <td>
            <div class="form-group row">
                <form action="{{ route('admin.update')}}" method="get">
                    @csrf
                    <div class="col">
                        <input type="hidden" name="code-edit" value="{{$value->product_code}}">
                        <button type="submit"  class="btn btn-warning btn-xs" >Edit</button>
                    </div>
                </form>
                <div class="col">
                    <button type="button"  class="btn btn-success btn-xs delele-button" id="delele-button" value="{{$value->product_code}}">Delete</button>
                </div>
            </div>
        </td>
        </tr>
        @endforeach
        @endif
    </tbody>
    </table>
    
</div>
<div class="container">
    {{ $allOrders->links() }}
</div>
@include('admin.partial.partial_change_status')
@include('admin.partial.partial_error')
@include('admin.partial.partial_add_product')

@stop
@section('js')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.bundle.min.js" integrity="sha512-iceXjjbmB2rwoX93Ka6HAHP+B76IY1z0o3h+N1PeDtRSsyeetU3/0QKJqGyPJcX63zysNehggFwMC/bi7dvMig==" crossorigin="anonymous"></script> --}}
{{-- <script type="text/javascript" src="{{asset('js/bootstrap.bundle.min.js') }}"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script> 
<script type="text/javascript" src="{{asset('js/admin.js') }}"></script>
    {{-- <link rel="script" src="{{asset('js/admin.js') }}"> --}}
@endsection