<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
class ProductImport implements ToModel,WithChunkReading, WithHeadingRow
{
    // public function headingRow() : int
    // {
    //     return 1;
    // }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Product([
            'product_code'      => $row['product_code'],
            'product_name'      => $row['product_name'],
            'product_jan'       => $row['product_jan'],
            'maker_full_nm'     => $row['maker_full_nm'],
            'process_status'    => $row['process_status'],
            'cheetah_status'    => $row['cheetah_status'],
        ]);
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
