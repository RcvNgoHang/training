<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'mst_users';
    protected $fillable = [
        'remember_token', 'email', 'password', 'is_active', 'last_login_at','last_login_ip'
    ];

    protected function updateData($email, $fillable){
        Users::where('email', $email)->update($fillable);
    }
}
