<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProductExport implements FromCollection, WithHeadings
{
    private  $data;
    public function __construct( $data ){
        $this->data = $data;
       
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        // dd($this->data,Product::getData( [
        //     'isJoin' => true
        // ], $this->data));
        return Product::getData( [
            'isJoin' => true
        ], $this->data);
        //return Product::where('product_code', '0001050560')->get();
    }

    //Thêm hàng tiêu đề cho bảng
    public function headings() :array {
    	return ["ステータス", "メーカー名", "品番", "JANコード", "商品名", "定価", "処理ステータス"];
    }

    
}
