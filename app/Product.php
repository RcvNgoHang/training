<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'mst_product';
    protected $fillable = [
        'product_code','price_supplier_id', 'product_name', 'product_jan', 'maker_cd', 
        'list_price','product_maker_code', 'brand_name', 'maker_full_nm', 'cheetah_status',
        'process_status', 'in_date', 'up_date'
    ];
    public $timestamps = false;
    protected function getData($fillable, $datasearch){
        $query = Product::select('mst_product.cheetah_status',
        'mst_product.maker_full_nm','mst_product.product_code',
        'mst_product.product_jan','mst_product.product_name'
        ,'mst_product.list_price')
                ->whereIn('process_status', array(0,1, 2) ) ;

       
        if ( isset($datasearch['cheetah-status'])  && !empty($datasearch['cheetah-status'] ) ) {
            
            $query->whereIn('cheetah_status', $datasearch['cheetah-status'] ) ;
        }

        if ( isset($datasearch['product-jan'])  && !empty($datasearch['product-jan'] ) ) {
            $keywords = explode(' ', $datasearch['product-jan'] );
            foreach ($keywords as $key => $keyword){
                if($key == 0) {
                    
                    $query->where('product_jan', 'like', '%'.$keyword.'%');
                   
                }else{
                    $query->orWhere('product_jan', 'like', '%'.$keyword.'%');
                }
            }
        }

        if ( isset($datasearch['poduct-maker-full'])  && !empty($datasearch['poduct-maker-full'] ) ) {
             $keywords = explode(' ', $datasearch['poduct-maker-full'] );
            foreach ($keywords as $key => $keyword){
                if($key == 0) {
                    $query->where('maker_full_nm', 'like', '%'.$keyword.'%');
                }else{
                    $query->orWhere('maker_full_nm', 'like', '%'.$keyword.'%');
                }
               
            }
        }

        if ( isset($datasearch['product-code'])  && !empty($datasearch['product-code'] ) ) {
            $keywords = explode(' ', $datasearch['product-code'] );
            
            foreach ($keywords as $key => $keyword){
                if($key == 0) {
                    $query->where('mst_product.product_code', 'like', '%'.$keyword.'%');
                }else{
                    $query->orWhere('mst_product.product_code', 'like', '%'.$keyword.'%');
                }
                
            }
            
        }

        if ( isset($datasearch['product-name'])  && !empty($datasearch['product-name'] ) ) {
            $keywords = explode(' ', $datasearch['product-name'] );
           
            foreach ($keywords as $key => $keyword){
                if($key == 0) {
                    $query->where('product_name', 'like', '%'.$keyword.'%');
                }else{
                    $query->orWhere('product_name', 'like', '%'.$keyword.'%');
                }
              
            }
        }

        if( isset( $fillable['isJoin']) && !empty( $fillable['isJoin'] ) ){
            $query->join('mst_users', 'mst_users.t_admin_id', '=', 'mst_product.price_supplier_id');
        }
        return $query->get();
    }

    protected function updateData($id, $dataUpdate){
        
        return  Product::whereIn('product_code', $id)
            ->update([
                'cheetah_status'    =>  $dataUpdate['cheetahStatus'],
                'process_status'    => 1
            ]);
        
    }
}
