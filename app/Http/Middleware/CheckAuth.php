<?php

namespace App\Http\Middleware;

use Closure;

class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->cookie('cookie_user') || $request->session()->get('session') ){
            return $next($request);
            
        }
        return redirect()->route('login');
       
    }
}
