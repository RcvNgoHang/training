<?php

namespace App\Http\Controllers;
use App\Product;
use DB;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Exports\ProductExport;
use App\Imports\ProductImport;
use Maatwebsite\Excel\Facades\Excel;
use Session;
class AdminController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    public function index(Request $request)
    {   
        
        $data =  $request->get('cheetah-status', '');
      
        $datasearch = array_intersect_key(
            $request->input(), [
                'cheetah-status'    => '',
                'product-jan'       => '',
                'poduct-maker-full' => '',
                'product-code'      => '',
                'product-name'      => ''
            ]
        );
        Session::put('session_search',$datasearch);
        $fill = [
            'isJoin' => true
        ];
        $quertResult    = Product::getData($fill, $datasearch);
        $totalGroup     = count($quertResult);
        $perPage        = 50;
        $page           = Paginator::resolveCurrentPage('page');
        $allOrders      = new LengthAwarePaginator($quertResult->forPage($page, $perPage), $totalGroup, $perPage, $page, [
            'path'      => Paginator::resolveCurrentPath(),
            'pageName'  => 'page',
        ]);
        
        return view('admin.admin', compact('allOrders', 'quertResult' ) );
    }

    //change status process and cheetah status in product
    /**
     * * change status process and cheetah status in product
     * @author Hang Ngo
     * @param id,status
     */
    public function changeStatus(Request $request){
        $id     = $request->input('id', '');
        $status = $request->input('status', '$status');
        $processStatus = 1 ;
        if( $request->ajax() && !empty($id) && !empty($status) )  {
            $cheetahStatus  = $this->getStatus($status);
            $query = Product::updateData( $id, ['processStatus' => $processStatus, 'cheetahStatus' => $cheetahStatus]);
            return response()->json([
                'status'    => true, 
                'message'   => 'Update data success !!'
            ]);
        }else{
            return response()->json([
                'status'  => false,
                'note'    => 'Success',
                'message' => 'Please try again !'
                ]);
        }
    }

    public function getStatus($status)
    {
        $cheetahStatus ;
        switch ($status) {
            case 'change-status':
                $cheetahStatus = 2;
                break;
            case 'status-two' :
                $cheetahStatus = 1;
                break;
            default:
                $cheetahStatus = 0;
                break;
        }
        return $cheetahStatus;
    }

    /**
     * * Export data flow search query
     * @author Hang Ngo
     * 
     */
    public function export(  Request $request){
        $datasearch= [];
        if ( !empty( $request->session()->has('session_search') 
            &&  !empty($request->session()->get('session_search'))) ) {
            $datasearch = $request->session()->get('session_search');
        }else{
             $datasearch = array_intersect_key(
                [
                    'cheetah-status'    => $request->input('cheetahStatus', ''),
                    'product-jan'       => $request->input('productJan', ''),
                    'poduct-maker-full' => $request->input('poductMakerFull', ''),
                    'product-code'      => $request->input('productCode', ''),
                    'product-name'      => $request->input('productName', '')
                ]
                , [
                    'cheetah-status'    => '',
                    'product-jan'       => '',
                    'poduct-maker-full' => '',
                    'product-code'      => '',
                    'product-name'      => ''
                ]
            );
        }
        
        
        $fill = [
            'isJoin' => true
        ];

        $export = new ProductExport( $datasearch );
        return Excel::download($export, 'product.xlsx');
       
    }

    /**
     * * Import data from excel file
     *  @author Hang Ngo
     */
    public function import(Request $request){
        $file = $request->file('user_file');
        Excel::import(new ProductImport, $file);
        return back();
    }

    /**
     * * delete product function
     * @author Hang Ngo
     * @param string product_code value
     */
    public function deleteProduct( Request $request )
    {
        $id = $request->input('id', '');
        if( $request->ajax() && !empty($id)
        &&   !empty( Product::where('product_code',$id)->first() ) )  {
            Product::where('product_code', $id)->delete();
            return response()->json([
                'status'    => true, 
                'message'   => 'Deleted !'
            ]);
        }else{
            return response()->json([
                'status'    => false, 
                'message'   => 'Please try again !'
            ]);
        }
    }

    /**
     * * add product function
     * @author Hang Ngo
     * @param array: $data 
     */
    public function addProduct( Request $request ){
       
        $data = $request->all();
        
        if($request->isMethod('post')){
            $validator = $request->validate([
                'product-code'          => 'required',
                'product-name'          => 'required',
                'product-jan'           => 'required',
                'cheetah-status'        => 'required',
                'process-status'        => 'required',
                'maker-full-nm'     => 'required'
            ], [
                'product-code.required'         => 'Product code is required',
                'product-name.required'         => 'Product name is required',
                'product-jan.required'          => 'Product jan is required',
                'cheetah-status.required'       => 'cheetah-status is required',
                'process-status.required'       => 'process-status is required',
                'maker-full-nm.required'      => 'Poduct-namemaker jan is required'
            ]);

            $productCheck = DB::table('mst_product')
                ->where('product_code', $data['product-code'])
                ->first();
                
            if( empty($productCheck) ){
                $product = new Product();
                $product->product_code      = $data['product-code'];
                $product->product_name      = $data['product-name'];
                $product->product_jan       = $data['product-jan'];
                $product->cheetah_status    = $data['cheetah-status'];
                $product->process_status    = $data['process-status'];
                $product->maker_full_nm     = $data['maker-full-nm'];
                $product->save();
                
                return redirect('/admin')->with('status', 'add sucssess');
            
            }
        }
        return redirect('/admin')->with('status', 'add fail data');
    }

    public function viewUpdate(  Request $request ){
        $idCode = $request->input('code-edit', '');
        $product = Product::where('product_code', $idCode)->first();
        return view('admin.update', compact('product') );
    }

    /**
     * * update product function
     * @author Hang Ngo
     * @param array(): $data
     */
     public function updateProduct( Request $request ){
        $idCode = $request->input('code-edit', $request->input('product-code', ''));
        $product = Product::where('product_code', $idCode)->first();
        if( $request->isMethod('post') ){
            $validator = $request->validate([
                'product-code'          => 'required',
                'product-name'          => 'required',
                'product-jan'           => 'required',
                'cheetah-status'        => 'required',
                'process-status'        => 'required',
                'maker-full-nm'     => 'required'
            ], [
                'product-code.required'         => 'Product code is required',
                'product-name.required'         => 'Product name is required',
                'product-jan.required'          => 'Product jan is required',
                'cheetah-status.required'       => 'cheetah-status is required',
                'process-status.required'       => 'process-status is required',
                'maker-full-nm.required'      => 'Poduct-namemaker jan is required'
            ]);

            $productCheck = DB::table('mst_product')
                ->where('product_code',  $idCode)
                ->first();
           
            if( !empty($productCheck) ){
                Product::where('product_code', $idCode)->update([
                    'product_code'      => $validator['product-code'],
                    'product_name'      => $validator['product-name'],
                    'product_jan'       => $validator['product-jan'],
                    'cheetah_status'    => $validator['cheetah-status'],
                    'process_status'    => $validator['process-status'],
                    'maker_full_nm'     => $validator['maker-full-nm'],

                ]);
                return redirect('/admin')->with('status', 'update sucssess');
            }
            return redirect('/admin')->with('status', 'update false');
        }

        return view('admin.update', compact('product') );
     }
}

