<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Users;
use Cookie;
use DB;
use Session;
class LoginController extends Controller
{
    /**
     * * Login pages function
     * @author Hang Ngo
     * @param string email, password
     */
    public function login(Request $request){
        
        
        if( $request->cookie('cookie_user') || $request->session()->get('session') ){
            return redirect()->route('admin.dashboard');
        }
       
        $email      = $request->post('email','');
        $pass       = $request->post('password','');
        $remeber    = $request->post('remember-check','');
        
        if($request->isMethod('post')){
            $validator = $request->validate([
                'password'  => 'required|min:5',
                'email'     => 'required|email'
            ], [
                'email.required'    => 'Email is required',
                'email.email'       => 'The email must be a valid email',
                'password.required' => 'Password is required'
            ]);

            $user = DB::table('mst_users')
                ->where('email', $email)
                ->first();
            
            if( !empty($user) &&  Hash::check( $pass, $user->password) ){
                if( !empty($remeber) ){
                    Cookie::queue('cookie_user', 'cookie_'.$email, 43200);
                }
                $remember_token = Hash::make(Session::put('session', 'session_'.$email));
                $ip = $this->getIPAddress();
                $dataUpdate = [
                    'remember_token'    => $remember_token,
                    'last_login_at'     => date("Y-m-d",time()),
                    'last_login_ip'     => $this->getIPAddress()
                ];
                $update = Users::updateData($email, $dataUpdate);
               
                return  redirect()->route('admin.dashboard');
            }else{
                return redirect()->route('login')->with('status', 'password or email incorrect');
            }
            
            
        }
        
        return view('admin.login');
    }

    // getIP function
    public function getIPAddress()
    {
        //whether ip is from the share internet  
        if(!empty($_SERVER['HTTP_CLIENT_IP'])) {  
            $ip = $_SERVER['HTTP_CLIENT_IP'];  
        }  
        //whether ip is from the proxy  
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {  
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];  
        }  
        //whether ip is from the remote address  
        else{  
            $ip = $_SERVER['REMOTE_ADDR'];  
        }  
        return $ip;  
    }


     /**
     * * Logout pages function
     * @author Hang Ngo
     * 
     */
    public function logout( Request $request ){
        $request->session()->forget('session');
        $request->session()->flush();
        Cookie::queue(Cookie::forget('cookie_user'));
        return redirect()->route('login');
    }
}

