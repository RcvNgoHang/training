
jQuery(document).ready(function($) { 
   
    $('#reset-form').on('click', function(){
        $('#search-form').trigger("reset").submit();
    });
     
    $('#change-status, #status-two, #status-three').on('click', function(){
        var id = [];
        $('input[name="status-change"]:checked').each(function() {
            id.push(this.value);
        });
        if (typeof id !== 'undefined' && id.length > 0) {
            var idCheetahStatus = $(this).attr('id');
            $('#cheetahStatus').find('#id-change').val(idCheetahStatus)
            $('#cheetahStatus').modal('show');
            //submit-action
        }else{
            $('#modelError').modal('show');
        }
    });
    $('#submit-action').on('click', function(e){
        var id = [];
        var status;
        $('input[name="status-change"]:checked').each(function() {
            id.push(this.value);
        });
        status = $('#id-change').val()
        // e.preventDefault();
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $.ajax({
             type:'POST',
             url: '/change-status' , 
             data:{id:id, status: status},
             success:function(data){
                $('#cheetahStatus').modal('hide');
                console.log(data.status);
                if(data.status == true){
                    swal({
                        title: 'Success',
                        type: "warning",
                        text: data.message,
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                      },
                      function(){
                        swal.close();
                        location.reload();
                      });
                      
                }
             },error:function(e){
                $('#cheetahStatus').modal('hide');
             }
          });
     });

     //export file
    $('#export-csv').on('click', function(){
        var cheetahStatus = [];
        var  productJan, poductMakerFull,
        productCode, productName;
        
        // $('input[class="cheetah-status"]:checked')
        $('.cheetah-status:checkbox:checked').each(function() {
            cheetahStatus.push(this.value);
        });
        productJan      = $('#product-jan').val();
        poductMakerFull = $('#poduct-maker-full').val();
        productCode     = $('#product-code').val();
        productName     = $('#product-name').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type:'POST',
            url: '/export-csv' , 
            data:{cheetahStatus:cheetahStatus, productJan: productJan,
                poductMakerFull: poductMakerFull, productCode:productCode,
                productName:productName    
            },
            success:function(data){
               if(data.status == true){
                   swal({
                       title: 'Success',
                       type: "warning",
                       text: data.message,
                       confirmButtonText: "OK",
                       closeOnConfirm: false
                     },
                     function(){
                       swal.close();
                       location.reload();
                     });
               }
            },error:function(e){
               $('#cheetahStatus').modal('hide');
            }
         });
    })
    
    //delete product
    $('.delele-button').on('click', function(){
        var id      = this.value;
        swal({
            title: "Are you sure?",
            text: "Your will not be able to recover this row!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
          },
          function(){
            console.log(id);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type:'POST',
                url: '/delete-product' , 
                data:{id:id, status: status},
                success:function(data){
                   console.log(data.status);
                   if(data.status == true){
                       swal({
                           title: 'Success',
                           type: "warning",
                           text: data.message,
                           confirmButtonText: "OK",
                           closeOnConfirm: false
                         },
                         function(){
                           swal.close();
                           location.reload();
                         });
                         
                   }else{
                        swal("Fail!", "Try again later", "error");
                   }
                },error:function(e){
                    swal("Fail!", "Try again later", "error");
                }
             });


            
          });
          
        
    })

    //add new product
    $('#add-new-product').on('click', function(){
        $('#addProduct').modal('show');
    })

    $('#btn-add-product').on('click', function(){
        var flex = true;
        var productCode = $.trim($('#addProductForm').find('input[name="product-code"]').val())
        console.log(productCode);
        if( $.trim($('#addProductForm').find('input[name="product-code"]').val()) == ''){
            flex = false;
        }
        if( $.trim($('#addProductForm').find('input[name="product-name"]').val()) == ''){
            flex = false;
        }
        if( $.trim($('#addProductForm').find('input[name="product-jan"]').val()) == ''){
            flex = false;
        }
        if( $.trim($('#addProductForm').find('input[name="cheetah-status"]').val()) == ''){
            flex = false;
        }
        if( $.trim($('#addProductForm').find('input[name="process-status"]').val()) == ''){
            flex = false;
        }
        if( $.trim($('#addProductForm').find('input[name="maker-full-nm"]').val()) == ''){
            flex = false;
        }

        if(flex == true){
            $('#addProductForm').submit();
        }else{
            swal("Fail!", "Please fill out full the form", "error");
        }

    })
});
