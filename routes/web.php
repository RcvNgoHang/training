<?php
use App\Http\Middleware\CheckAuth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::any('/', 'LoginController@login')->name('login');

//Route::any('/admin','AdminController@index')->name('dashboard');
//Route::any('/admin', ['as' => 'admin.dashboard', 'uses' => 'AdminController@index'])->middleware(CheckAuth::class);
Route::group(['middleware' => 'checkauth'], function () {
    // All route your need authenticated
    Route::any('/admin', ['as' => 'admin.dashboard', 'uses' => 'AdminController@index']);
    Route::post('/change-status', 'AdminController@changeStatus')->name('admin.ajax');
    Route::any('/export-csv', 'AdminController@export')->name('admin.export');
    Route::post('/import-csv', 'AdminController@import')->name('admin.import');
    Route::post('/delete-product', 'AdminController@deleteProduct')->name('admin.ajax.delete');
    Route::post('/admin/add-product', 'AdminController@addProduct')->name('admin.addProduct');
    Route::any('/admin/edit-product', 'AdminController@updateProduct')->name('admin.update');
    Route::post('/admin/page-edit-product', 'AdminController@viewUpdate')->name('admin.viewUpdate');
    Route::any('/logout', 'LoginController@logout')->name('logout');
});
